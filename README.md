README
-----------

   This offers a dockerized version of FEniCS.

CONTACT
-----------

   Lizao "Larry" Li (lixx1445 at umn edu).

DOCUMENTATION
-----------
   
   1. Install docker on your platform (see https://docs.docker.com/). 
      Then pull the latest FEniCS image:

        docker pull lzlarryli/fenics:latest

   2. ONLY for the first run, create a virtual machine (may need sudo on 
      linux):

        docker run --name fenics -d -p 127.0.0.1:40:22 lzlarryli/fenics

      whichmaps port 22 (default for ssh) of the virtual machine to port 
      40 of the host. This allows only local access. If instead the 
      following is used:

        docker run --name fenics -d -p 40:22 lzlarryli/fenics

      then any computer in the host's network can access it at host_ip:40 
      (for example, for remote access or MPI jobs).

   3. Log into the virtual machine:

        ssh -Y root@127.0.0.1 -p 40

      The default password is "fenics". If only local access is allowed, 
      there is no need to change the password (it cannot be empty). If 
      network access is allowed in step 2, then the password should be 
      changed. To change the password, just run "passwd". For better 
      security, password login should be disabled and certificate based 
      login should be used as usual.

   4. Run FEniCS. It is installed in /opt. By default, "fenics.conf" is 
      already sourced. Try some demos at "/opt/FEniCS/share/dolfin/demo/".
      Plotting should work. The default home folder is "/data".

      IMPORTANT: "/data" is different from all other folders as it is 
      persistent. Files in "/data" will be transferred to the newer 
      version of FEniCS without any file operation (for upgrades see 7.)
   
      Once done, exit ssh. To stop the virtual machine daemon, run:

        docker stop fenics

      To start the virtual machine again, run:

        docker start fenics

      A typical work flow is 
      
        (start fenics) -> (ssh into it) -> (work) -> (exit ssh)
      
      When you shut down the computer, the daemon will be 
      stopped automatically and safely.

   5. File sharing between the virtual machine and the host. This is the
      same as sharing files with remote servers. scp will work. A more 
      convenient solution is sshfs. Suppose sshfs is installed. Create 
      a mount point (a folder) in the virtual machine, say,
      
        mkdir /opt/fenics
        
      then run the following on the host:
      
        sudo sshfs -o allow_other root@127.0.0.1:/data /opt/fenics -p 40
        
      This links "/opt/fenics" on the host to "/data" in the virtual 
      machine.
   
   6. FEniCS development. The dorsal building script used can be found 
      in "/opt/fenics-project-dorsal-xxxx". In case part of FEniCS needs
      to be rebuild. Simply go there, skip unmodified components in the 
      platform file "FEniCS/platforms/supported/trusty.platform" and 
      dorsal it.

   7. Upgrade to a new version of FEniCS. Make sure all the files to
      survive is in the folder "/data". It is probably a good idea to 
      make a backup as well. Then run:

        docker stop fenics 
	    docker tag lzlarryli/fenics:latest fenics:old
	    docker pull lzlarryli/fenics:latest
	    docker run --name fenics-temp --volumes-from fenics -d -p 22 lzlarryli/fenics
        docker rm fenics
        docker run --name fenics --volumes-from fenics-temp -d -p 127.0.0.1:40:22 lzlarryli/fenics
	    docker stop fenics-temp
	    docker rm fenics-temp
	    docker rmi fenics:old
	    docker start fenics

      The above commands renames the local lzlarryli/fenics:latest image 
      to fenics:old. Then the new version is pulled in. Currently docker 
      cannot rename virtual machines. So a virtual machine "fenics-temp"
      is created and the "/data" folder is transferred to it first. Then
      the old "fenics" machine is deleted and the new "fenics" is 
      started with the data from "fenics-temp". Finally "fenics-temp" 
      and the old image are deleted. This is efficient because here
      "--volumes-from" just passes a link and files are never copied.
      
   8. Uninstall. Run:
        
        docker stop fenics
        docker rm fenics
        docker rmi lzlarryli/fenics
        
      Alternatively, if you use docker only with this. You can run:
      
        docker stop $(docker ps -a -q)
        docker rm $(docker ps -a -q)
        docker rmi $(docker images -q)
        
      and then uninstall docker.