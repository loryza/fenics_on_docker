FROM ubuntu:14.04
MAINTAINER Lizao Li <lixx1445@umn.edu>

# Setup persistent volume
VOLUME /home

# Install packages. The last line is from the dorsal platform file for ubuntu.
RUN apt-get -qq update && \
    apt-get -qqy install ssh unzip emacs23-nox ipython openssh-server xauth ed sshfs git && \
    apt-get -qqy install bzr bzrtools cmake flex g++ gfortran libarmadillo-dev libatlas-base-dev libboost-dev libboost-filesystem-dev libboost-iostreams-dev libboost-mpi-dev libboost-program-options-dev libboost-thread-dev libboost-math-dev libboost-timer-dev libboost-chrono-dev libcln-dev libcppunit-dev libginac-dev liblapack-dev libmpfr-dev libopenmpi-dev libptscotch-dev libsuitesparse-dev libxml2-dev openmpi-bin pkg-config python-dev python-numpy python-scientific python-vtk subversion swig wget bison libhwloc-dev python-ply libvtk5-dev python-netcdf libhdf5-openmpi-dev libeigen3-dev libcgal-dev 

# Setup up ssh.
# Create home directory if it does not exist for a user, this is needed because 
# the home directory is in a persistent volume and has to be created after the image
# is built. 
# User fenics does not need password to wrong sudo, for convenience.
# /opt, where FEniCS is going to be installed, is owned by user fenics.
RUN sed -i 's|AcceptEnv|#AcceptEnv|' /etc/ssh/sshd_config && \
    mkdir /var/run/sshd && \
    echo 'Name: activate mkhomedir' > /usr/share/pam-configs/my_mkhomedir && \
    echo 'Default: yes' >> /usr/share/pam-configs/my_mkhomedir && \
    echo 'Priority: 900' >> /usr/share/pam-configs/my_mkhomedir && \
    echo 'Session-Type: Additional' >> /usr/share/pam-configs/my_mkhomedir && \
    echo 'Session:' >> /usr/share/pam-configs/my_mkhomedir && \
    echo '   required pam_mkhomedir.so umask=0022 skel=/etc/skel' >> /usr/share/pam-configs/my_mkhomedir && \
    echo 'session required   pam_mkhomedir.so umask=0022 skel=/etc/skel' >> /etc/pam.d/common-session && \
    useradd -s /bin/bash -G sudo fenics && \
    echo 'fenics:fenics' | chpasswd && \
    sed -i -e 's|%sudo   ALL=(ALL:ALL) ALL|sudo ALL=NOPASSWD:ALL|g' /etc/sudoers && \
    chown fenics:fenics /opt 
    echo "source /opt/FEniCS/share/dolfin/dolfin.conf" >> /etc/bash.bashrc && \
    echo "export ENV LIBGL_ALWAYS_INDIRECT=y" >> /etc/bash.bashrc 
    
# Use dorsal to compile FEniCS
USER fenics:fenics
WORKDIR /opt
RUN wget https://bitbucket.org/fenics-project/dorsal/get/19472eb6161f.zip && \
    unzip 19472eb6161f.zip
WORKDIR /opt/fenics-project-dorsal-19472eb6161f
RUN sed -i 's|PROCS=1|PROCS=8|' ./dorsal.cfg
RUN sed -i 's|${HOME}/Work|/opt|' ./dorsal.cfg && \
    sed -i 's|STABLE_BUILD=true|STABLE_BUILD=false|' ./dorsal.cfg && \
    cp ./FEniCS/platforms/supported/trusty.platform ./build.platform && \
    sed -i 's|trilinos|#trilinos|' ./build.platform && \
    sed -i 's|petsc|petsc\npetsc4py|' ./build.platform && \
    ./dorsal.sh ./build.platform && \

# Default command to run
CMD ["/usr/sbin/sshd", "-D"] 
